package funciones;

import modelo.Personas;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Ing. Diego Romero
 * @version 2.0
 * @fecha 2016-08-06
 * @correo info@diego.ec
 *
 */
public class FuncionesTabla {

    public ArrayList<Personas> consultaPersonas(Connection c) {
        String consulta = "SELECT * FROM personas";
        ArrayList<Personas> personas = new ArrayList<>();
        try {
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(consulta);
            Personas aux;
            while (rs.next()) {
                aux = new Personas(
                        rs.getInt("id_persona"),
                        rs.getString("dni"),
                        rs.getString("nombres"),
                        rs.getString("apellidos"),
                        rs.getString("telefono"),
                        rs.getString("correo"),
                        rs.getDate("fecha_nacimiento"),
                        rs.getDate("fecha_hora_registro"),
                        rs.getDate("fecha_hora_modificacion")
                );
                personas.add(aux);
            }
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        }
        return personas;
    }

    public Boolean actualizaCorreo(Connection c, Integer id, String correo) {
        String consulta = "UPDATE personas SET correo='" + correo + "' WHERE id_persona=" + id;

        try {
            Statement s = c.createStatement();
            s.executeUpdate(consulta);
            return true;
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }

    }

    public Boolean actualizaCelular(Connection c, Integer id, String tel) {
        String consulta = "UPDATE personas SET telefono='" + tel + "' WHERE id_persona=" + id;

        try {
            Statement s = c.createStatement();
            s.executeUpdate(consulta);
            return true;
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }

    public void llenarTabla(JTable j, Object personas[][]) {
        j.setModel(new DefaultTableModel(personas, new String[]{
            "Id", "DNI", "Nombres", "Apellidos", "Correo", "Teléfono", "Fecha Nacimiento", "Fecha Registro", "Fecha Modificación"
        }) {
            Class[] types = new Class[]{
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean[]{
                false, false, false, false, true, true, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        if (j.getColumnModel().getColumnCount() > 0) {
            j.getColumnModel().getColumn(0).setPreferredWidth(5);//id
            j.getColumnModel().getColumn(1).setPreferredWidth(35);//dni
            j.getColumnModel().getColumn(2).setPreferredWidth(80);
            j.getColumnModel().getColumn(3).setPreferredWidth(80);
            j.getColumnModel().getColumn(4).setPreferredWidth(120);//correo
            j.getColumnModel().getColumn(5).setPreferredWidth(35);//tel
            j.getColumnModel().getColumn(6).setPreferredWidth(40);
            j.getColumnModel().getColumn(7).setPreferredWidth(90);
            j.getColumnModel().getColumn(8).setPreferredWidth(90);
        }

    }

    public Object[][] arrayToMatriz(ArrayList<Personas> per) {
        Object[][] personas = new Object[per.size()][9];
        for (int i = 0; i < per.size(); i++) {
            personas[i][0] = per.get(i).getIdPersona();
            personas[i][1] = per.get(i).getDni();
            personas[i][2] = per.get(i).getNombres();
            personas[i][3] = per.get(i).getApellidos();
            personas[i][4] = per.get(i).getCorreo();
            personas[i][5] = per.get(i).getTelefono();
            personas[i][6] = new SimpleDateFormat("yyyy-MM-dd").
                    format(per.get(i).getFechaNacimiento());
            personas[i][7] = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").
                    format(per.get(i).getFechaHoraRegistro());
            personas[i][8] = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").
                    format(per.get(i).getFechaHoraModificacion());

        }
        return personas;
    }

}
