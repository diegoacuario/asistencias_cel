package funciones;

import vista.VentanaRegistrarAsistencia;
import java.sql.Connection;

/**
 *
 * @author Ing. Diego Romero
 * @version 2.0
 * @fecha 2016-08-06
 * @correo info@diego.ec
 *
 */
public class ActualizaHora extends Thread {

    Connection c;
    VentanaRegistrarAsistencia ra;

    public ActualizaHora(Connection c) {
        this.c = c;
        ra = new VentanaRegistrarAsistencia(c);
        ra.setVisible(true);
    }

    public void run() {
        while (true) {
            ra.setTitle("Asistencias " + Funciones.obtieneHora(c)+" versión 3.0 12-2016");
            try {
                sleep(1000);
            } catch (InterruptedException ex) {

            }
        }

    }
}
