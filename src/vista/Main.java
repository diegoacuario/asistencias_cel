package vista;


import funciones.ActualizaHora;
import bd.ConexionBD;
import java.sql.Connection;

/**
 *
 * @author Ing. Diego Romero
 * @version 2.0
 * @fecha 2016-08-06
 * @correo info@diego.ec
 *
 */
public class Main {

    public static void main(String[] args) {
        Connection c = ConexionBD.getConecion();
        new ActualizaHora(c).start();

    }

}
